import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Smoke {

    private FirefoxDriver driver;

    @BeforeClass
    public void setUp() {
        FirefoxDriverManager.getInstance().arch64().version("0.19.1").setup();
        FirefoxOptions ffOptions = new FirefoxOptions();
        ffOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        driver = new FirefoxDriver(ffOptions);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test
    public void case_Title() {
        driver.get("https://ya.ru");
        Assert.assertTrue(driver.getTitle().contains("Яндекс"));
    }

    @AfterClass
    public void tearDown() throws Exception {
        driver.quit();
    }
}
